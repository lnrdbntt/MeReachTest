package com.meseems.mereach.app.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.meseems.mereach.R;
import com.meseems.mereach.domain.entity.MRServer;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Sacra on 23/10/16.
 */
public class ServerAdapter extends RecyclerView.Adapter<ServerAdapter.ViewHolder> {

    private List<MRServer> listServer;
    private Context context;

    public ServerAdapter(List<MRServer> listServer, Context context) {
        this.listServer = listServer;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_main_list_server_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MRServer server = this.listServer.get(position);

        holder.textViewName.setText(server.getServerName());

        if (server.getReachable()) {
            holder.textViewStatus.setText(context.getResources().getString(R.string.reachability_reachable));
            holder.textViewStatus.setTextColor(ContextCompat.getColor(context, R.color.green));
        } else {
            holder.textViewStatus.setText(context.getResources().getString(R.string.reachability_not_reachable));
            holder.textViewStatus.setTextColor(ContextCompat.getColor(context, R.color.red));
        }

        holder.textViewLastUpdate.setText(server.getLastUpdateDate());
    }

    @Override
    public int getItemCount() {
        return this.listServer.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.activity_main_list_server_item_name)
        TextView textViewName;

        @BindView(R.id.activity_main_list_server_item_status)
        TextView textViewStatus;

        @BindView(R.id.activity_main_list_server_item_last_update)
        TextView textViewLastUpdate;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
