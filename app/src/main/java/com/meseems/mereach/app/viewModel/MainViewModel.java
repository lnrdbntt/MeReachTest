package com.meseems.mereach.app.viewModel;

import com.meseems.mereach.domain.entity.MRServer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sacra on 23/10/16.
 */
public class MainViewModel {

    private List<MRServer> listServer;

    public MainViewModel() {
        this.listServer = new ArrayList<>();
    }

    public List<MRServer> getListServer() {
        return listServer;
    }

    public void setListServer(List<MRServer> listServer) {
        this.listServer = listServer;
    }
}
