package com.meseems.mereach.app.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.widget.EditText;

import com.jakewharton.rxbinding.support.v4.widget.RxSwipeRefreshLayout;
import com.jakewharton.rxbinding.view.RxView;
import com.meseems.mereach.R;
import com.meseems.mereach.app.adapter.ServerAdapter;
import com.meseems.mereach.app.controller.MainController;
import com.meseems.mereach.app.viewModel.MainViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.functions.Action1;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.activity_main_list_server)
    RecyclerView recyclerViewServer;

    @BindView(R.id.activity_main_button_add_domain)
    FloatingActionButton buttonAddDomain;

    @BindView(R.id.activity_main_edit_text_domain)
    EditText editTextDomain;

    @BindView(R.id.activity_main_list_swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private MainViewModel mainViewModel;

    private MainController mainController;

    private ServerAdapter serverAdapter;

    private Action1<Integer> actionAddItemRecyclerView;
    private Action1<Integer> actionRemoveItemRecyclerView;
    private Action1<Integer> actionUpdateItemRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.mainViewModel = new MainViewModel();
        this.mainController = new MainController(this.mainViewModel, this);

        ButterKnife.bind(this);

        this.initActions();
        this.initRecyclerView();
        this.initUIInteraction();

        this.mainController.startUpdateTimer(actionUpdateItemRecyclerView);
    }

    /**
     * Initializes actions to be subscribed by controller methods to update the layout
     */
    private void initActions() {
        this.actionAddItemRecyclerView = new Action1<Integer>() {
            @Override
            public void call(Integer i) {
                serverAdapter.notifyItemInserted(i);
                serverAdapter.notifyItemRangeChanged(i, mainViewModel.getListServer().size());

                editTextDomain.setText("");
            }
        };

        this.actionRemoveItemRecyclerView = new Action1<Integer>() {
            @Override
            public void call(Integer i) {
                serverAdapter.notifyItemRemoved(i);
                serverAdapter.notifyItemRangeChanged(i, mainViewModel.getListServer().size());
            }
        };

        this.actionUpdateItemRecyclerView = new Action1<Integer>() {
            @Override
            public void call(Integer i) {
                serverAdapter.notifyItemChanged(i);
            }
        };
    }

    /**
     * Initializes the server adapter and adds it to the recycler view
     */
    private void initRecyclerView() {
        this.serverAdapter = new ServerAdapter(this.mainViewModel.getListServer(), this);
        this.recyclerViewServer.setAdapter(serverAdapter);
        this.recyclerViewServer.setLayoutManager(new LinearLayoutManager(this));
    }

    /**
     * Initializes all activity UI interaction
     */
    private void initUIInteraction() {
        /**
         * Initializes swipe gesture to delete items on recycler view
         */
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                mainController.removeServer(viewHolder.getLayoutPosition(), actionRemoveItemRecyclerView);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(this.recyclerViewServer);

        /**
         * Binds add domain button on click action
         */
        RxView.clicks(this.buttonAddDomain).subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
                mainController.addNewServer(editTextDomain.getText().toString(), actionAddItemRecyclerView);
            }
        });

        /**
         * Initializes the pull to refresh action
         */
        RxSwipeRefreshLayout.refreshes(this.swipeRefreshLayout).subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
                mainController.updateServerListStatus(actionUpdateItemRecyclerView);

                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }
}
