package com.meseems.mereach.app.controller;

import android.content.Context;

import com.meseems.mereach.app.viewModel.MainViewModel;
import com.meseems.mereach.domain.DataStore.MRServerDataSource.MRIServerDataSource;
import com.meseems.mereach.domain.DataStore.MRServerDataSource.MRServerDumbPersistence;
import com.meseems.mereach.domain.entity.MRServer;
import com.meseems.mereach.domain.helper.MRDateHelper;
import com.meseems.mereach.domain.networking.MRReachabilityService;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Sacra on 25/10/16.
 */
public class MainController {

    private MainViewModel mainViewModel;
    private MRIServerDataSource serverDataSource;

    public MainController(MainViewModel mainViewModel, Context context) {
        this.mainViewModel = mainViewModel;
        this.serverDataSource = new MRServerDumbPersistence(context);

        /**
         * Retrieves the saved list
         */
        this.mainViewModel.setListServer(this.serverDataSource.getServers());
    }

    /**
     * Adds a new server object based on the domain entered
     */
    public void addNewServer(String domain, final Action1<Integer> actionUpdateRecyclerView) {
        if (domain.isEmpty()) {
            return;
        }

        final MRServer server = new MRServer();
        server.setServerName(domain);

        /**
         * Validates if the domains is reachable before adding to the list and updating the recycler view
         */
        new MRReachabilityService()
                .isReachable(server.getServerName())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Boolean>() {

                    @Override
                    public void call(Boolean isReachable) {
                        server.setReachable(isReachable);
                        server.setLastUpdateDate(MRDateHelper.getCurrentDate());
                        mainViewModel.getListServer().add(server);

                        /**
                         * For every change on the server list, persists it
                         */
                        serverDataSource.saveServer(server);

                        Observable.just(mainViewModel.getListServer().size() - 1).subscribe(actionUpdateRecyclerView);
                    }
                });
    }

    /**
     * Removes the server specified by the position and updates the recycler view
     */
    public void removeServer(int position, Action1<Integer> actionUpdateRecyclerView) {
        this.mainViewModel.getListServer().remove(position);

        /**
         * For every change on the server list, persists it
         */
        this.serverDataSource.removeServer(null);

        Observable.just(position).subscribe(actionUpdateRecyclerView);
    }

    /**
     * For each server saved, updates its reachability and update date and updates the recycler view
     */
    public void updateServerListStatus(final Action1<Integer> actionUpdateRecyclerView) {
        Observable.from(this.mainViewModel.getListServer())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<MRServer>() {
                    @Override
                    public void call(final MRServer mrServer) {
                        new MRReachabilityService()
                                .isReachable(mrServer.getServerName())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Action1<Boolean>() {

                                    @Override
                                    public void call(Boolean isReachable) {
                                        mrServer.setReachable(isReachable);
                                        mrServer.setLastUpdateDate(MRDateHelper.getCurrentDate());

                                        /**
                                         * For every change on the server list, persists it
                                         */
                                        serverDataSource.updateServer(mrServer);

                                        Observable.just(mainViewModel.getListServer().indexOf(mrServer)).subscribe(actionUpdateRecyclerView);
                                    }
                                });
                    }
                });

    }

    /**
     * Starts a timer to update the server list status every 10 seconds
     */
    public void startUpdateTimer(final Action1<Integer> actionUpdateRecyclerView) {
        Observable.interval(10000, TimeUnit.MILLISECONDS)
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        updateServerListStatus(actionUpdateRecyclerView);
                    }
                });
    }
}
