package com.meseems.mereach.domain.helper;

import java.util.Calendar;

/**
 * Created by Sacra on 26/10/16.
 */
public class MRDateHelper {

    static public String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        String currentDate = c.get(Calendar.HOUR) + ":" + c.get(Calendar.MINUTE) + ":" + c.get(Calendar.SECOND);

        return currentDate;
    }
}
