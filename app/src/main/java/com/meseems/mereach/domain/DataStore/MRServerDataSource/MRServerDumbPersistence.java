package com.meseems.mereach.domain.DataStore.MRServerDataSource;

import android.content.Context;

import com.meseems.mereach.domain.entity.MRServer;
import com.meseems.mereach.domain.vendor.TinyDB;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sacra on 26/10/16.
 */
public class MRServerDumbPersistence implements MRIServerDataSource {

    private final String TinyDBPersistenceKey = "MRServerList";

    private TinyDB tinyDB;
    private List<MRServer> listServer;

    public MRServerDumbPersistence(Context context) {
        this.tinyDB = new TinyDB(context);

        this.initListServer();
    }

    private void initListServer() {
        this.listServer = new ArrayList<>();
        this.listServer = (List<MRServer>) (Object) tinyDB.getListObject(TinyDBPersistenceKey, MRServer.class);
    }

    private void persistData() {
        this.tinyDB.putListObject(TinyDBPersistenceKey, new ArrayList<Object>(this.listServer));
    }

    @Override
    public List<MRServer> getServers() {
        return this.listServer;
    }

    @Override
    public void saveServer(MRServer server) {
//        this.listServer.add(server);
        this.persistData();
    }

    @Override
    public void removeServer(MRServer server) {
//        this.listServer.remove(server);
        this.persistData();
    }

    @Override
        public void updateServer(MRServer server) {
        this.persistData();
    }
}
