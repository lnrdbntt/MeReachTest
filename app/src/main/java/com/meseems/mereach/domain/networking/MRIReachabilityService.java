package com.meseems.mereach.domain.networking;

import rx.Observable;

/**
 * Created by nickmm on 8/23/16.
 */
public interface MRIReachabilityService {
    Observable<Boolean> isReachable(String serverUrl);
}
