package com.meseems.mereach.domain.DataStore.MRServerDataSource;

import com.meseems.mereach.domain.entity.MRServer;

import java.util.List;

/**
 * Created by Sacra on 26/10/16.
 */
public interface MRIServerDataSource {
    List<MRServer> getServers();
    void saveServer(MRServer server);
    void removeServer(MRServer server);
    void updateServer(MRServer server);
}
