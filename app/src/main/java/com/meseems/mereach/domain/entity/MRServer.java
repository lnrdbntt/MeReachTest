package com.meseems.mereach.domain.entity;

/**
 * Created by Sacra on 23/10/16.
 */
public class MRServer {

    private String serverName;
    private Boolean isReachable;
    private String lastUpdateDate;

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public Boolean getReachable() {
        return isReachable;
    }

    public void setReachable(Boolean reachable) {
        isReachable = reachable;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }
}
